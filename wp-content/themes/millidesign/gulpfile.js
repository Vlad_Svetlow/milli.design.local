var postCss = require('gulp-postcss'),
    sass = require('gulp-sass'),
    sourceMaps = require('gulp-sourcemaps'),
    gulp = require('gulp'),
    debug = require('gulp-debug'),
    clean = require('gulp-clean'),
    autoPrefixer = require('autoprefixer'),
    mqPacker = require('css-mqpacker'),
    fileInclude = require('gulp-file-include'),
    watch = require('gulp-watch'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

path = {
  build: {
    html: '',
    js: 'js/',
    css: 'library/css/',
   //postStyle: 'dist/css/main.css',
    img: 'img/',
    fonts: 'fonts/'
  },
  src: {
    html: 'includes_html/*.html',
    js: 'assets/js/**/*',
    style: 'library/scss/*.scss',
    img: 'assets/img/**/*',
    fonts: 'assets/fonts/**/*'
  },
  watch: {
    html: 'includes_html/*.html',
    js: 'js/**/*.js',
    style: 'library/scss/**/*.scss',
    img: 'img/**/*.*',
    fonts: 'fonts/**/*.*'
  },
  clean: './dist'
};

var processors = [
  autoPrefixer({
    browsers: ['> 1%']
  }),
  mqPacker({
    sort: true
  })
];

config = {
  server: {
    baseDir: './'
  },
  index: 'index.html',
  tunnel: true,
  host: 'localhost',
  port: 9000,
  logPrefix: 'millidesign'
};

gulp.task('html:build', function() {
  gulp.src(path.src.html)
      .pipe(fileInclude({
        prefix: '@@',
        basepath: '@file'
      }))
      .pipe(gulp.dest(path.build.html))
      .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
  gulp.src(path.src.style)
      .pipe(sourceMaps.init())
      .pipe(sass({
        outputStyle: 'expanded'
      }).on('error', sass.logError))
      .pipe(postCss(processors))
      .pipe(sourceMaps.write('./maps'))
      .pipe(gulp.dest(path.build.css))
      .pipe(reload({stream: true}));
});

gulp.task('js:build', function() {
  gulp.src(path.src.js)
      .pipe(gulp.dest(path.build.js));
  //.pipe(reload({stream: true})); //slow build js
});

gulp.task('image:build', function () {
  gulp.src(path.src.img)
      .pipe(gulp.dest(path.build.img));
});

gulp.task('fonts:build', function() {
  gulp.src(path.src.fonts)
      .pipe(gulp.dest(path.build.fonts));
});


gulp.task('build', [
  //'html:build',
  'style:build'
  //'js:build',
  //'image:build',
  //'fonts:build'
]);

gulp.task('watch', function(){

  // watch([path.watch.html], function(event, cb) {
  //   gulp.start('html:build');
  // });

  watch([path.watch.style], function(event, cb) {
    gulp.start(['style:build']);
  });

  //watch([path.watch.img], function(event, cb) {
  //  gulp.start(['image:build']);
  //});

  //watch([path.watch.js], function(event, cb) {
  //  gulp.start(['js:build']);
  //});

});

gulp.task('webserver', function () {
  browserSync(config);
});

//gulp.task('clean', function () {
//  gulp.src(path.clean, {read: false})
//      .pipe(clean());
//});

gulp.task('default', ['build', 'webserver', 'watch']);