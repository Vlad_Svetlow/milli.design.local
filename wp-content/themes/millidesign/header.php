<!doctype html>

<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php wp_title(''); ?></title>

	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">

	<meta name="msapplication-TileColor" content="#f01d4f">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
					<meta name="theme-color" content="#121212">

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php wp_head(); ?>

	<?php // drop Google Analytics Here ?>
	<?php // end analytics ?>

</head>

<body <?php body_class(); ?>>

<?php
	global $cfs_fields,
				 $cfs_props;

	$cfs_fields = CFS()->get();
	$cfs_props = CFS()->get_field_info();
?>

	<!-- Header -->
	<header id="header">
		<nav class="bNavBarHead navbar"> <!--navbar-default-->
			<div class="container">

				<div class="navbar-header">
					<button type="button" class="eToggleBtn navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarCollapse"
									aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="bBrand navbar-brand" href="/"><?php echo $cfs_fields['f_m_logo_title']; ?> <strong><?php echo $cfs_fields['f_m_logo_descr']; ?></strong></a>
				</div>

				<div class="collapse navbar-collapse" id="navbarCollapse">
					<ul id="mainNav" class="bNavList nav navbar-nav">
						<li>
							<a href="#banner"><i class="fa fa-home"></i></a>
						</li>

						<?php
						$cfs_head_menu = $cfs_fields['f_head_menu'];

						foreach ($cfs_head_menu as $field) {

							echo '<li><a href="#' . $field['f_head_menu_anchor'] . '">' . $field['f_head_menu_item'] . '</a></li>';
						}
						?>
					</ul>

					<ul class="bContactsList nav navbar-nav">
						<li>
							<a href="javascript:void(0);"><i class="fa fa-phone"></i> <?php echo $cfs_fields['f_head_tel'] ?></a>
						</li>
						<li>
							<a href="javascript:void(0);"><i class="fa fa-clock-o"></i> <?php echo $cfs_fields['f_head_time'] ?></a>
						</li>
					</ul>

					<div class="eNavBarBtn">
						<button type="button" class="bBtn mLead btn navbar-btn" data-toggle="modal" data-target="#modalCall"><?php echo $cfs_fields['f_head_btn'] ?></button>
					</div>

				</div>


			</div>
		</nav>
	</header>
	<!-- END Header -->


	<!-- Preloader -->
	<div id="pagePreloader" class="bPreloader">
		<div class="bPreloaderContent">
			<div class="bBrand navbar-brand"><?php echo $cfs_fields['f_m_logo_title']; ?> <strong><?php echo $cfs_fields['f_m_logo_descr']; ?></strong></div>

			<div>
				<svg version="1.1" id="L7" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
     <path d="M31.6,3.5C5.9,13.6-6.6,42.7,3.5,68.4c10.1,25.7,39.2,38.3,64.9,28.1l-3.1-7.9c-21.3,8.4-45.4-2-53.8-23.3
      c-8.4-21.3,2-45.4,23.3-53.8L31.6,3.5z" transform="rotate(9 50 50)">
			 <animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="2s" from="0 50 50" to="360 50 50" repeatCount="indefinite"></animateTransform>
		 </path>
					<path d="M42.3,39.6c5.7-4.3,13.9-3.1,18.1,2.7c4.3,5.7,3.1,13.9-2.7,18.1l4.1,5.5c8.8-6.5,10.6-19,4.1-27.7
      c-6.5-8.8-19-10.6-27.7-4.1L42.3,39.6z" transform="rotate(-18 50 50)">
						<animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="1s" from="0 50 50" to="-360 50 50" repeatCount="indefinite"></animateTransform>
					</path>
					<path d="M82,35.7C74.1,18,53.4,10.1,35.7,18S10.1,46.6,18,64.3l7.6-3.4c-6-13.5,0-29.3,13.5-35.3s29.3,0,35.3,13.5
      L82,35.7z" transform="rotate(9 50 50)">
						<animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="2s" from="0 50 50" to="360 50 50" repeatCount="indefinite"></animateTransform>
					</path>
    </svg>
			</div>
		</div>

	</div>
	<!-- END Preloader -->
