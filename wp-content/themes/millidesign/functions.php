<?php

require_once( 'library/theme.php' );

/*********************
LAUNCH THEME
Let's get everything up and running.
*********************/

function launch_theme () {

  // launching operation cleanup
  add_action( 'init', 'theme_head_cleanup' );

  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'theme_remove_wp_widget_recent_comments_style', 1 );

  // clean up comment styles in the head
  add_action( 'wp_head', 'theme_remove_recent_comments_style', 1 );

  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'theme_scripts_and_styles', 999 );

  // cleaning up random code around images
  add_filter( 'the_content', 'theme_filter_ptags_on_images' );

  // cleaning up excerpt
  add_filter( 'excerpt_more', 'theme_excerpt_more' );

} /* end bones ahoy */

// let's get this party started
add_action( 'after_setup_theme', 'launch_theme' );


/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'theme-thumb-600', 600, 150, true );
add_image_size( 'theme-thumb-300', 300, 100, true );

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 100 sized image,
we would use the function:
<?php the_post_thumbnail( 'theme-thumb-300' ); ?>
for the 600 x 150 image:
<?php the_post_thumbnail( 'theme-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

add_filter( 'image_size_names_choose', 'theme_custom_image_sizes' );

function theme_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'theme-thumb-600' => __('600px by 150px'),
        'theme-thumb-300' => __('300px by 100px'),
    ) );
}

/* Hide Admin Bar */
show_admin_bar(false);