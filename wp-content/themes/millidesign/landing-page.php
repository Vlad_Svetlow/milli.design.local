<?php
/*
 * Template Name: Landing Page
*/
?>


<?php get_header(); ?>

  <!-- Content Wrapper -->
  <div class="contentWrap">

    <!-- Section Banner -->
    <section id="banner" class="secBanner mBlockMiddle" style="background: url('<?php echo $cfs_fields['f_sec_top_bg']; ?>') center / cover no-repeat;">
      <div class="mBlockMiddleTarget">
        <div class="container">
          <h1 class="wow fadeInDown" data-wow-duration="2s"><?php echo $cfs_fields['f_sec_top_title_1']; ?></h1>

          <p class="wow fadeInDown" data-wow-duration="2s"><?php echo $cfs_fields['f_sec_top_title_2']; ?></p>

          <h2 class="wow fadeInUp" data-wow-duration="2s"><?php echo $cfs_fields['f_sec_top_title_3']; ?></h2>

          <div>
            <button type="button" class="bBtn mLead btn wow fadeInUp" data-wow-duration="2s" data-toggle="modal" data-target="#modalCall">Отправить заявку</button>
          </div>
        </div>
      </div>
    </section>
    <!-- END Section Banner -->

    <!-- Section About Us -->
    <section id="<?php echo $cfs_fields['f_sec1_menu_anchor']?>" class="secAboutUs">
      <div class="container">
        <h2 class="wow fadeInDown" data-wow-duration="1s"><?php echo $cfs_fields['f_sec1_title']?></h2>

        <div class="bFeaturesCircle wow fadeInUp" data-wow-duration="1s">
          <div class="row">

            <?php
            $cfs_sec1_features = $cfs_fields['f_sec1_features'];

            foreach ($cfs_sec1_features as $field) {

              echo '<div class="col-sm-6 col-md-3">
                        <div class="bItemCircle">
                            <div class="eIcon iconMicroscope"></div>
                            <h3 class="eTitle">'. $field['f_sec1_feature_descr'] .'</h3>
                        </div>
                    </div>';
            }
            ?>

          </div>
        </div>
      </div>

      <!-- Features Slider -->
      <div id="features" class="secFeatures">
        <div id="sliderFeatures">
          <?php
            $cfs_slider = $cfs_fields['f_slider'];

            foreach ($cfs_slider as $field) :
          ?>
          <!-- Slider Item -->
          <div class="bItemFeature item mBlockMiddle" style="background: url('<?php echo $field['f_slider_bg']; ?>') center / cover no-repeat;">
            <article class="mBlockMiddleTarget">
              <div class="bDescr">
                <div class="container">
                  <div class="row">
                    <div class="col-sm-6 col-md-6 hidden-xs text-center">
                      <img src="<?php echo $field['f_slider_thumb']; ?>" alt="">
                    </div>

                    <div class="col-sm-6 col-md-6">
                      <h2 class="eTitle"><?php echo $field['f_slider_title']; ?></h2>

                      <p><?php echo $field['f_slider_descr']; ?></p>
                      <a class="eMore" href="#"><?php echo $cfs_fields['f_slider_btn']; ?></a>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </div>
          <!-- END Slider Item -->
          <?php endforeach; ?>

        </div>

        <div id="sliderControls" class="bSliderControls hidden-xs">
          <div class="ePrev"><i class="fa fa-angle-left"></i></div>
          <div class="eNext"><i class="fa fa-angle-right"></i></div>
        </div>

      </div>
      <!-- END Features Slider -->
    </section>
    <!-- END Section About Us -->

    <!-- Section Portfolio -->
    <section id="<?php echo $cfs_fields['f_sec2_menu_anchor']?>" class="secPortfolio">

      <div class="container">
        <h2 class="wow fadeInDown" data-wow-duration="1s"><?php echo $cfs_fields['f_sec2_title']?></h2>

        <h3 class="wow fadeInUp" data-wow-duration="1s"><?php echo $cfs_fields['f_sec2_descr']?></h3>
      </div>

      <div class="bPortfolioItems wow fadeIn" data-wow-duration="1s" >
        <div class="clearfix">

        <?php
          $cfs_portfolio = $cfs_fields['f_portfolio'];

          foreach ($cfs_portfolio as $field) :
        ?>
          <div class="bPItem">
            <figure>
              <p class="eThumb"><img src="<?php echo $field['f_portfolio_thumb']; ?>" alt=""></p>
              <figcaption class="bDescr">
                <div class="eLink">
                  <a href="javascript:void(0);">
                  <span class="fa-stack">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-link fa-stack-1x"></i>
                  </span>
                  </a>
                </div>
                <div class="eText mVCenter">
                  <div class="mVCenterTarget">
                    <h4><?php echo $field['f_portfolio_author']; ?></h4> <?php echo $field['f_portfolio_descr']; ?>
                  </div>
                </div>
              </figcaption>
            </figure>
          </div>
        <?php endforeach; ?>

        </div>

      </div>


    </section>
    <!-- END Section Portfolio -->

    <!-- Section Prices -->
    <section id="<?php echo $cfs_fields['f_sec3_menu_anchor']; ?>" class="secPrices">
      <div class="container">
        <h2 class="wow fadeInDown" data-wow-duration="1s"><?php echo $cfs_fields['f_sec3_title']; ?></h2>

        <h3 class="wow fadeInUp" data-wow-duration="1s"><?php echo $cfs_fields['f_sec3_descr']; ?></h3>

        <div class="bPrices">
          <div class="row">

            <?php
              $cfs_cart = $cfs_fields['f_p_cart'];

              foreach ($cfs_cart as $field) :
            ?>

              <div class="col-md-4">
                <div class="bPriceCart <?php echo ($field['f_p_cart_lead']) ? 'mCartLead' : ''; ?> wow zoomIn" data-wow-duration="1s" data-wow-delay="<?php
                  $order = $field['f_p_cart_sort'];
                  $val = array('first' => '', 'second' => '0.3s', 'third' => '0.6s');

                  foreach ($order as $key => $label ) {
                    $delay = $val[$key];
                  }

                  echo $delay;

                ?>">

                  <div class="bHeader">
                    <h4 class="eTitle"><?php echo $field['f_p_cart_title']; ?></h4>
                  </div>
                  <div class="bContent">

                    <div class="bPriceTitle">

                      <?php
                          $price = explode('.', $field['f_p_cart_price']);
                      ?>
                      <p class="ePrice"><?php echo $price[0] ?><span><b><?php echo $price[1] ?></b><?php

                          $currency = $field['f_p_cart_price_val'];

                          foreach ( $currency as $key => $label ) {
                            echo '<i class="fa fa-' . $key . '"></i>';
                          }
                        ?></span></p>

                      <p class="eDescr"><?php echo $field['f_p_cart_price_descr']; ?></p>
                    </div>


                    <div class="eLine"></div>



                      <ul class="bList list-unstyled">
                        <?php
                        $cfs_cart_list = $field['f_p_cart_list'];

                        foreach ($cfs_cart_list as $sub_field) :
                        ?>
                        <li><?php echo $sub_field['f_p_cart_list_feature'] ?></li>
                        <?php endforeach; ?>
                      </ul>

                    <div class="bBotBtn">
                      <button type="button" class="bBtn <?php echo ($field['f_p_cart_lead']) ? 'mLead' : 'mDef'; ?> btn" data-toggle="modal" data-target="#modalCall"><?php echo $cfs_fields['f_sec3_btn']; ?></button>
                    </div>
                  </div>
                </div>
              </div>

            <?php endforeach; ?>

          </div>
        </div>
      </div>


    </section>
    <!-- END Section Prices -->

    <!-- Section Contacts -->
    <section id="contacts" class="secContacts">

      <!--@TODO: move to admin -->
      <div id="gMap" class="bMap"></div>

      <div>
        <form action="/"></form>
      </div>

      <script src='https://maps.googleapis.com/maps/api/js?key=&sensor=false&extension=.js'></script>

      <script>
        google.maps.event.addDomListener(window, 'load', init);
        var map;
        function init() {
          var mapOptions = {
            center: new google.maps.LatLng(40.746867,-434.17039),
            zoom: 12,
            zoomControl: true,
            zoomControlOptions: {
              style: google.maps.ZoomControlStyle.DEFAULT,
            },
            disableDoubleClickZoom: true,
            mapTypeControl: false,
            scaleControl: true,
            scrollwheel: false,
            panControl: true,
            streetViewControl: true,
            draggable : true,
            overviewMapControl: true,
            overviewMapControlOptions: {
              opened: false,
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}],
          }
          var mapElement = document.getElementById('gMap');
          var map = new google.maps.Map(mapElement, mapOptions);
          var locations = [

          ];
          for (i = 0; i < locations.length; i++) {
            if (locations[i][1] =='undefined'){ description ='';} else { description = locations[i][1];}
            if (locations[i][2] =='undefined'){ telephone ='';} else { telephone = locations[i][2];}
            if (locations[i][3] =='undefined'){ email ='';} else { email = locations[i][3];}
            if (locations[i][4] =='undefined'){ web ='';} else { web = locations[i][4];}
            if (locations[i][7] =='undefined'){ markericon ='';} else { markericon = locations[i][7];}
            marker = new google.maps.Marker({
              icon: markericon,
              position: new google.maps.LatLng(locations[i][5], locations[i][6]),
              map: map,
              title: locations[i][0],
              desc: description,
              tel: telephone,
              email: email,
              web: web
            });
            link = '';     }

        }
      </script>
    </section>
    <!-- END Section Contacts -->

  </div>
  <!-- END Content Wrapper -->

  <!-- Modal Form -->
  <div id="modalCall" class="bModalForm modal fade" tabindex="-1" role="dialog">
    <div class="bModalDialog modal-dialog modal-sm">
      <div class="bModalContent modal-content">

        <button type="button" class="eCloseBtn" data-dismiss="modal" aria-label="Close">
        <span class="fa-stack">
          <i class="fa fa-circle fa-stack-2x"></i>
          <i class="fa fa-times fa-stack-1x"></i>
        </span>
        </button>


        <div class="bFormCall">
          <form action="/">
            <h5 class="eTitle">Заказать звонок</h5>

            <div class="bFormGr form-group">
              <label for="userName" class="sr-only">ФИО</label>
              <input type="text" class="form-control" id="userName" placeholder="ФИО">
            </div>

            <div class="bFormGr form-group">
              <label for="userTel" class="sr-only">Телефон</label>
              <input type="tel" class="form-control" id="userTel" placeholder="Телефон">
            </div>

            <div class="bFormGr form-group">
              <textarea rows="6" placeholder="Сообщение"></textarea>
            </div>

            <button type="submit" class="bBtn mLead mFullW btn">Отправить заявку</button>
          </form>
        </div>

      </div>
    </div>
  </div>
  <!-- END Modal Form -->

<?php get_footer(); ?>