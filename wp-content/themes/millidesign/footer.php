<?php
	global $cfs_fields,
				 $cfs_props;
?>

	<!-- Footer -->
	<footer>
		<div class="container">
			<div class="pull-left">
				<a class="bBrand navbar-brand" href="/"><?php echo $cfs_fields['f_m_logo_title']; ?> <strong><?php echo $cfs_fields['f_m_logo_descr']; ?></strong></a>
			</div>

			<div class="pull-right">
				<p class="copyright">&copy; <?php echo date('Y'); ?> - <?php bloginfo( 'name' ); ?>. Все права защищены</p>
			</div>
		</div>

	</footer>
	<!-- END Footer -->

	<?php wp_footer(); ?>

</body>

</html>
