<?php
/*
 * This is the core Theme
 *
*/

/*********************
WP_HEAD GOODNESS
*********************/

function theme_head_cleanup() {
	// category feeds
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
	// remove WP version from css
	add_filter( 'style_loader_src', 'theme_remove_wp_ver_css_js', 9999 );
	// remove Wp version from scripts
	add_filter( 'script_loader_src', 'theme_remove_wp_ver_css_js', 9999 );

} /* end theme head cleanup */

// remove WP version from scripts
function theme_remove_wp_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}

// remove injected CSS for recent comments widget
function theme_remove_wp_widget_recent_comments_style() {
	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	}
}

// remove injected CSS from recent comments widget
function theme_remove_recent_comments_style() {
	global $wp_widget_factory;
	if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
		remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
	}
}


/*********************
SCRIPTS & ENQUEUEING
*********************/

// loading modernizr and jquery, and reply script
function theme_scripts_and_styles() {

  global $wp_styles;

  if (!is_admin()) {

		// register bootstrap stylesheet
		wp_register_style( 'twbs-stylesheet', get_stylesheet_directory_uri() . '/library/css/bootstrap.min.css', array(), '', 'all' );

		// register OWL carousel stylesheet
		wp_register_style( 'owl-stylesheet', get_stylesheet_directory_uri() . '/library/css/owl.carousel.css', array(), '', 'all' );

		// register font awesome stylesheet
		wp_register_style( 'fa-stylesheet', get_stylesheet_directory_uri() . '/library/css/font-awesome.min.css', array(), '', 'all' );

		// register animate stylesheet
		wp_register_style( 'animate-stylesheet', get_stylesheet_directory_uri() . '/library/css/animate.css', array(), '', 'all' );

		// register main stylesheet
		wp_register_style( 'theme-stylesheet', get_stylesheet_directory_uri() . '/library/css/main.css', array(), '', 'all' );


		//disallow jquery wp
		wp_deregister_script( 'jquery' );

		//register jquery cdn
		wp_register_script('jquery-cdn', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js', false, '1.11.2', false);

		// register bootstrap js
		wp_register_script( 'twbs-js', get_stylesheet_directory_uri() . '/library/js/bootstrap.min.js', array( 'jquery-cdn' ), '', true );

		// register owl carousel js
		wp_register_script( 'owl-js', get_stylesheet_directory_uri() . '/library/js/owl.carousel.min.js', array( 'jquery-cdn' ), '', true );

		// register wow js
		wp_register_script( 'wow-js', get_stylesheet_directory_uri() . '/library/js/wow.min.js', array( 'jquery-cdn' ), '', true );

		// register main js
		wp_register_script( 'theme-js', get_stylesheet_directory_uri() . '/library/js/main.js', array( 'jquery-cdn' ), '', true );

		// enqueue styles and scripts
		wp_enqueue_style('twbs-stylesheet');
		wp_enqueue_style('owl-stylesheet');
		wp_enqueue_style('animate-stylesheet');
		wp_enqueue_style('fa-stylesheet');
		wp_enqueue_style('theme-stylesheet');

		wp_enqueue_script( 'jquery-cdn' );
		wp_enqueue_script( 'twbs-js' );
		wp_enqueue_script( 'owl-js' );
		wp_enqueue_script( 'wow-js' );
		wp_enqueue_script( 'theme-js' );

	}
}


/*********************
OTHERS
**********************/

// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
function theme_filter_ptags_on_imagess($content){
	return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

// This removes the annoying […] to a Read More link
function theme_excerpt_more($more) {
	global $post;
	// edit here if you like
	return '...  <a class="excerpt-read-more" href="'. get_permalink( $post->ID ) . '" title="'. __( 'Read ', 'bonestheme' ) . esc_attr( get_the_title( $post->ID ) ).'">'. __( 'Read more &raquo;', 'bonestheme' ) .'</a>';
}



?>
