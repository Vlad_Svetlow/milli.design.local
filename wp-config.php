<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'milli_design');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'OgAxgUg0? &(;U2=;:9%v7gH~/?`?d>-!{LR42bGZ1|Uq)P.Yv!bE6ZP6=uQ1]jP');
define('SECURE_AUTH_KEY',  '#A!NYW)Cu<EUW2:G**;V-fx%d,!|:2Hhw|7(JFGmtTz-4zu601k_C}+8e7BE@&iG');
define('LOGGED_IN_KEY',    'LyB~nD76@Gh(@s>`a_=Z{NCfgo^$++J^;Y5t7+^YUa^Xl4uXLUKRau8W-odAqs2V');
define('NONCE_KEY',        ']fD:M[pHQUU9op)-%I[lxYj4%V`L7lQR0x9;%O:ZCflBC-}.Y|c<L:XEtbWCt$Gx');
define('AUTH_SALT',        'PLZ!e@J-+/IdhH :XCa+ta]ksp}VOwVY>=wKuxx6|<N!Zzb3l-SOX;p|j-Wwk_|7');
define('SECURE_AUTH_SALT', 'If@6(cq!PEkn{s3uDcHc_Pvh/|TqE_Vi-wLx@WV1}V~EBALP3$@+-,txNcpVE|$G');
define('LOGGED_IN_SALT',   'hN=gSBR09x7(!S?Q: G$j]VGL>/oIK#zH1 7M*yMeRIv#biw]9N]Av5Nl=,KQh_u');
define('NONCE_SALT',       '6adbFR9`44h{#B<vK2)61/G}C3/%qKE ;yOJHr@0Gw}hcw&6VO8)HV_PN.w|X}HZ');
/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
